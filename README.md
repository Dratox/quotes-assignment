# Quotes

This application has been built for the Kabisa coding assignment, by Kevin Pas.

The app primarily focuses on front-end development, through the use of the [VueJS](https://vuejs.org/v2/guide/)
framework. The initial setup has been done with the [VueCLI](https://cli.vuejs.org/guide/) webpack config.

The back-end server is very rudimentary and is only intended to supply the necessary features to support the front-end
application. Usually, an application like this would be paired with a proper back-end framework which is far more
flexible in its development. However, my usual back-end framework ([Laravel](https://laravel.com/docs/8.x)) is rather
time-consuming to set up on a new computer (and a little overkill), so I have opted to go for a light-weight approach
and used [NodeJS](https://nodejs.org/en/) instead.

At the end of this readme, you'll find a number of development ideas / considerations I've had before starting the
actual programming. I thought it might be interesting to include those.

---

## Requirements

```
- Node - version 8.9 or above (v10+ recommended)
- NPM
```

---

## Development

### Project setup

inside the project root folder, run the following commands to get started

```
# in project root folder
npm install
cp .env.example .env
```

### Compiles and hot-reloads for development

```
npm run development
```

### Start the Ratings server

```
# open a new terminal window in project root folder
npm run server
```

### Compiles and minifies for production

```
npm run build
```

---

## Deployment

Pull the repo to a server that meets the requirements and run `npm ci` and `npm run build` to compile the source files
into the minified `/dist` files directory.

Direct your domain to load the `/dist` folder by default. (for instance through NGINX config)

Run the server with `npm run server`, although it is highly recommended to run the server through a node process manager
like [pm2](https://pm2.keymetrics.io/)

--- 

## Development considerations

Based on the criteria for this assignment, I've made careful considerations about which features I was going to
implement. There are of course many more options when it comes to a conceptual assignment like this one. Below is a list
of potential features I have considered;

> ### Guess That Quote
> This was an idea to incorporate a simple version of a guessing trivia game. Users would get shown a quote,
> and get a multiple choice for whom they think said that quote.
> This feature would then have its own leaderboard (score + time), showing off each user's Trivia Prowess!

> ### AR (Artificial Reality)
> Pros: Users could use their (smartphone) camera and project Quotes on floor / wall surfaces, which they could see through their smartphone screen.
>
> Cons: There are a number of available ways to approach this, but there are notable differences between phones (android / iOS).
> As I can't reliably test each variant of these devices, I've opted not to go down this path.

> ### VR (Virtual Reality)
> Pros: Users could see a huge Quote, floating in 3D space right in front of their eyes.
>
> Cons: Would require the use of a VR Cardboard headset test out the solution, which I can't assume that every developer has available.
> A good framework for achieving VR on the web is ['A-Frame'](https://aframe.io/docs/1.2.0/introduction/).

> ### Back-end Framework
> To allow for complete persistence of the data, there would have to be a more extensive back-end server setup.
> This would include features such as a database, proper API endpoints.
> Potentially even User and Access Management for more elaborate features such as managing custom Quotes, approval workflows and much more.