module.exports = {
  chainWebpack: config => {
    config.plugin('html').tap(args => {
      args[0].template = 'src/index.html'
      args[0].favicon = 'src/favicon.ico'
      args[0].title = process.env.NODE_ENV === 'development' ? 'Quotes (DEV)' : 'Quotes'
      return args
    });
  },
  css: {
    loaderOptions: {
      scss: {
        prependData: `@import "@/design/_variables.scss";`
      },
    },
  },
}