const api = require('./api.js');
const dotenv = require('dotenv');

dotenv.config();

api.listen(process.env.VUE_APP_SERVER_PORT, () =>
  console.log(`Web API listening on port ${process.env.VUE_APP_SERVER_PORT}`),
);

