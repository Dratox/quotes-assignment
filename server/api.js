const express = require('express');
const api = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const Ratings = require('./ratings.js')

const ratings = new Ratings();

api.use(bodyParser.json())
api.use(cors())

/**
 * Endpoint for retrieving data of a single rating
 */
api.put('/rating', (req, res) => {

  if (!req.body.hasOwnProperty('token') ||
    !req.body.hasOwnProperty('quote_id') ||
    !req.body.hasOwnProperty('rating')) {
    return res.status(500).send('Missing parameters')
  }

  return res.send(ratings.setRating(
    req.body['token'],
    parseInt(req.body['quote_id']),
    parseInt(req.body['rating'])
  ))
})


/**
 * Endpoint for submitting or updating a new rating
 */
api.get('/rating', (req, res) => {

  if (!req.query.hasOwnProperty('token') || !req.query.hasOwnProperty('quote_id')) {
    return res.status(500).send('Missing parameters')
  }

  return res.send(ratings.getRating(
    req.query['token'],
    parseInt(req.query['quote_id'])
  ))
})


/**
 * Endpoint for retrieving all rating information, sorted by average rating score
 */
api.get('/ratings', (req, res) => {
  return res.send(ratings.getSortedRatings())
})


module.exports = api;