function Ratings() {

  this.quoteRatings = [];

}


/**
 * Get the rating item for a user, including the totals (average / votes)
 * @param _token
 * @param _quote_id
 * @returns {{}}
 */
Ratings.prototype.getRating = function getRating(_token, _quote_id) {
  let result = {}
  let ratingItem = this.quoteRatings.find(x => (x.token === _token && x.quote_id === _quote_id))
  if (ratingItem) {
    result['rating'] = ratingItem['rating']
  }
  result['totals'] = this.getTotals(_quote_id);
  return result;
}


/**
 * Set a new rating value, or update an existing one and return the item
 * return includes the totals (average / votes)
 * @param _token
 * @param _quote_id
 * @param _rating
 * @returns {{quote_id, rating, token}}
 */
Ratings.prototype.setRating = function setRating(_token, _quote_id, _rating) {

  let previous = this.quoteRatings.findIndex(x => {
    return x.token === _token && x.quote_id === _quote_id
  })

  let rating;

  if (previous === -1) {
    rating = {
      token: _token,
      quote_id: _quote_id,
      rating: _rating
    }
    this.quoteRatings.push(rating)
  } else {
    if (this.quoteRatings[previous]['rating'] === _rating) {
      this.quoteRatings[previous]['rating'] = null;
    } else {
      this.quoteRatings[previous]['rating'] = _rating
    }
    rating = this.quoteRatings[previous];
  }

  rating['totals'] = this.getTotals(_quote_id);

  return rating;
}


/**
 * Helper function for gathering the totals for a particular quote ID
 * @param quote_id
 * @returns {{}}
 */
Ratings.prototype.getTotals = function getTotals(quote_id) {
  let ratings = this.quoteRatings.filter(x => x.quote_id === parseInt(quote_id) && x.rating !== null).flatMap(x => x.rating)
  let result = {}
  result['votes'] = ratings.length;
  result['average'] = ratings.reduce((prev, curr) => prev + curr, 0) / ratings.length;
  return result;
}


/**
 * Groups each quote by ID, with the number of votes and average rate score, and returns these sorted.
 * @returns {*[]}
 */
Ratings.prototype.getSortedRatings = function getSortedRatings() {
  let ratings = [];

  this.quoteRatings.forEach(x => {
    if (!ratings.find(r => r.quote_id === x.quote_id)) {
      let totals = this.getTotals(x.quote_id);
      if (totals.average) {
        ratings.push({
          quote_id: x.quote_id,
          totals: totals
        })
      }
    }
  })

  return ratings.sort((a, b) => {
    if (a.totals.average < b.totals.average) return 1;
    if (a.totals.average > b.totals.average) return -1;
    return 0;
  })
}

module.exports = Ratings;
