// load in some extra packages that we need. FontAwesome in this case.

import Vue from 'vue';
import {library} from '@fortawesome/fontawesome-svg-core'
import {fas} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome'

library.add(fas);
Vue.component('awesome-icon', FontAwesomeIcon);