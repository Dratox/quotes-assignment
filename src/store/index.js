import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios';
import localQuotes from './quotes.json';

Vue.use(Vuex)

export default new Vuex.Store({

  state: {
    quotes: {},
    quoteIds: [],
    quoteIdsShuffled: [],

    userIdentifier: null,

    // key that we're going to use to store the generated User Identifier
    identifierKey: 'SphR$iCmatN2xXo&Wn9t'
  },

  actions: {

    getQuotes({commit}) {
      return axios.get('http://quotes.stormconsultancy.co.uk/quotes.json')
        .then(res => {
          commit('SET_QUOTES', {quotes: res.data})
        })
        .catch(() => {
          console.warn('Failed to load quotes from API. Setting locally stored list instead.')
          commit('SET_QUOTES', {quotes: localQuotes})
        })
    },

    setUserIdentifier({commit, state}) {
      return Promise.resolve()
        .then(() => {
          // check if we have a User Identifier set, otherwise generate a new one and store it
          let identifier = localStorage.getItem(state.identifierKey)
          if (!identifier) {
            identifier = uuidv4();
          }
          commit('SET_IDENTIFIER', {identifier})
        })
    },

    rateQuote({state}, {rating, quoteId}) {
      // rate a quote and receive its new status back from the server
      const payload = {
        token: state.userIdentifier,
        quote_id: parseInt(quoteId),
        rating: rating
      }
      return axios.put(process.env.VUE_APP_SERVER_URL + ':' + process.env.VUE_APP_SERVER_PORT + '/rating', payload)
        .then(res => {
          return res.data
        })
        .catch(err => {
          console.error(err)
        })
    },

    getRating({state}, {quoteId}) {
      // get the rating for the current quote from the server
      return axios.get(process.env.VUE_APP_SERVER_URL + ':' + process.env.VUE_APP_SERVER_PORT + '/rating?quote_id=' + quoteId + '&token=' + state.userIdentifier)
        .then(res => {
          return res.data;
        })
        .catch(err => {
          console.error(err)
        })
    },

    getRatings() {
      // get all current ratings from the server, grouped by Quote
      return axios.get(process.env.VUE_APP_SERVER_URL + ':' + process.env.VUE_APP_SERVER_PORT + '/ratings')
        .then(res => {
          return res.data
        })
        .catch(err => {
          console.error(err)
        })
    },

    getPrevious({state}, {quoteId}) {
      // get the ID of the previous quote in the shuffled array
      const current = state.quoteIdsShuffled.indexOf(parseInt(quoteId))
      const previousIndex = (current === 0) ? (state.quoteIdsShuffled.length - 1) : current - 1;
      return state.quoteIdsShuffled[previousIndex];
    },

    getNext({state}, {quoteId}) {
      // get the ID of the next quote in the shuffled array
      const current = state.quoteIdsShuffled.indexOf(parseInt(quoteId))
      const nextIndex = (current === state.quoteIdsShuffled.length - 1) ? 0 : current + 1;
      return state.quoteIdsShuffled[nextIndex];
    },

    shuffleQuotes({state, commit}, {previous}) {
      let quotes = shuffleArray([...state.quoteIds], previous)
      commit('SET_SHUFFLED', {quotes: quotes})
    },

    shuffleQuotesByRating({commit, dispatch, state}) {
      return dispatch('getRatings')
        .then(res => {
          let rated = res.flatMap(x => x.quote_id);
          let unrated = state.quoteIds.filter(x => !rated.includes(x))
          unrated = shuffleArray(unrated);

          commit('SET_SHUFFLED', {quotes: rated.concat(unrated)})
        })

    },
  },

  mutations: {
    SET_QUOTES(state, {quotes}) {
      quotes.forEach(x => {
        state.quotes[x.id] = x
        state.quoteIds.push(x.id);
      })
    },
    SET_SHUFFLED(state, {quotes}) {
      state.quoteIdsShuffled = quotes;
    },
    SET_IDENTIFIER(state, {identifier}) {
      state.userIdentifier = identifier;
      localStorage.setItem(state.identifierKey, identifier)
    },
  },
})

function shuffleArray(arr, previous = null) {
  const newArr = arr.slice()
  for (let i = newArr.length - 1; i > 0; i--) {
    const rand = Math.floor(Math.random() * (i + 1));
    [newArr[i], newArr[rand]] = [newArr[rand], newArr[i]];
  }
  if (newArr[0] === previous) {
    return shuffleArray(newArr, previous)
  }
  return newArr
}

function uuidv4() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}
