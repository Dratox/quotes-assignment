import Vue from 'vue'
import VueRouter from 'vue-router'
import Quotes from '../views/Quotes.vue'
import Quote from '../views/Quote.vue'
import Leaderboard from '../views/Leaderboard.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/quotes',
    meta: {name: 'quote'},
    component: Quotes,
    children: [
      {
        path: '/quotes/:quote',
        meta: {name: 'quote'},
        component: Quote
      }
    ],
  },
  {
    path: '/leaderboard',
    meta: {name: 'leaderboard'},
    component: Leaderboard,
  },
  {
    path: '*',
    redirect: '/quotes'
  }
]

const router = new VueRouter({
  routes,
  scrollBehavior() {
    // scroll to the top of the page on a route change
    return {x: 0, y: 0}
  },
})

export default router
